package objects;

import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.collection.Array;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;
import transforms.Point3D;

public class Cube implements Model<Point3D, Topology> {
    private final @NotNull IndexedSeq<Point3D> vertices;
    private final @NotNull IndexedSeq<Integer> indices;
    private final @NotNull Seq<Tuple3<Integer, Integer, Topology>> groups;

    public Cube() {
        vertices = Array.of(
                new Point3D(0,0,0),
                new Point3D(1,0,0),
                new Point3D(1,1,0),
                new Point3D(0,1,0),
                new Point3D(0,0,1),
                new Point3D(1,0,1),
                new Point3D(1,1,1),
                new Point3D(0,1,1)
        );
        indices = Stream.rangeClosed(0, 3).flatMap(
                i -> Array.of(
                        i, (i + 1) % 4,
                        i, i + 4,
                        i + 4, (i + 1) % 4 + 4
                    )
        ).toArray();
        groups = Array.of(Tuple.of(0, 12, Topology.LINE_LIST));
    }

    @NotNull
    @Override
    public IndexedSeq<Point3D> getVertices() {
        return vertices;
    }

    @NotNull
    @Override
    public IndexedSeq<Integer> getIndices() {
        return indices;
    }

    @NotNull
    @Override
    public Seq<Tuple3<Integer, Integer, Topology>> getGroups() {
        return groups;
    }
}
