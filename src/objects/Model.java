package objects;

import io.vavr.Tuple3;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Seq;
import org.jetbrains.annotations.NotNull;

public interface Model<VertexType, TopologyType> {
    @NotNull IndexedSeq<VertexType> getVertices();
    @NotNull IndexedSeq<Integer> getIndices();

    /**
     * Returns a sequence of group, each having start index,
     * primitive count and topology
     * @return group description as a tuple of start index,
     * number of primitives, topology
     */
    @NotNull Seq<Tuple3<Integer, Integer, TopologyType>> getGroups();
}
