package objectops;

import io.vavr.collection.IndexedSeq;
import objects.Model;
import org.jetbrains.annotations.NotNull;
import rasterdata.Raster;
import transforms.Mat4;

public interface Renderer<PixelType, VertexType, TopologyType> {
    default @NotNull Raster<PixelType> render(
            @NotNull Raster<PixelType> background,
            @NotNull Model<VertexType, TopologyType> model,
            @NotNull Mat4 transform,
            @NotNull PixelType value
    ) {
        return model.getGroups().foldLeft(background,
            (currentImage, group) -> render(
                    currentImage,
                    model.getVertices(),
                    model.getIndices(),
                    group._1, //start index
                    group._2, //primitive count
                    group._3, //topology
                    transform,
                    value
                )
        );
    }
    @NotNull Raster<PixelType> render(
            @NotNull Raster<PixelType> background,
            @NotNull IndexedSeq<VertexType> vertices,
            @NotNull IndexedSeq<Integer> indices,
            int startIndex,
            int primitiveCount,
            @NotNull TopologyType topology,
            @NotNull Mat4 transform,
            @NotNull PixelType value
    );
}
