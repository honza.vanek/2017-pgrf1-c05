import objectops.RenderWireframe;
import objects.Cube;
import org.jetbrains.annotations.NotNull;
import rasterdata.*;
import rasterops.HeWhoDrawsLines;
import rasterops.HeWhoDrawsLinesNaively;
import rasterops.SeedFill4;
import transforms.Camera;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;

	private @NotNull Raster<Color> raster;
	private final @NotNull Presenter<Graphics, Color> presenter;
	private final @NotNull HeWhoDrawsLines<Color> lineGuru;

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
/*
		raster = new FastRaster<>(img,
					col -> col.getRGB()
					,
					intCol -> new Color(intCol)
		);
		presenter = new FastPresenter<>();
/*/
		raster = new VavrRaster<>(width, height, new Color(0xFF000000));
		presenter = new SlowPresenter<>(
				col -> col.getRGB()
		);
//*/
		lineGuru = new HeWhoDrawsLinesNaively<>();
		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		raster = raster.cleared(new Color(0xff2f2f2f));
	}

	public void present(final Graphics graphics) {
//		graphics.drawImage(img, 0, 0, null);
		presenter.present(graphics, raster);
	}

	public void draw() {
		clear();
		raster = raster.withValue(10,10, new Color(0xffffff00));
		/*
		raster = lineGuru.draw(raster,
				-0.9, 0.8, 0.9, -0.8,
				new Color(0xffff00ff));
		raster = lineGuru.draw(raster,
				-0.9, -0.8, 0.9, 0.8,
				new Color(0xffff00ff));
		raster = lineGuru.draw(raster,
				-0.9, -0.1, 0.9, -0.1,
				new Color(0xffff00ff));
		raster = new SeedFill4<Color>().fill(raster,
					raster.getWidth() / 2, raster.getHeight() / 2 + 5,
					new Color(0xffffff00),
					pixel -> pixel.equals(new Color(0xff2f2f2f))
				);
		*/
		raster =
				new RenderWireframe<Color>(lineGuru)
						.render(raster, new Cube(),
								new Camera()
										.withPosition(new Vec3D(5,3,2))
										.withAzimuth(Math.PI)
										.withZenith(-Math.atan(2.0 / 5.0))
										.getViewMatrix()
										.mul(new Mat4PerspRH(
												Math.PI / 3,
												raster.getHeight() / (double) raster.getWidth(),
												0.1, 1000
										)),
								new Color(1.0f, 1, 0));


//		img.setRGB(10, 10, 0xffff00);
	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}