package rasterops;

import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;
import rasterdata.Raster;

public class HeWhoDrawsLinesNaively<PixelType> implements HeWhoDrawsLines<PixelType> {
    @NotNull
    @Override
    public Raster<PixelType> draw(
            @NotNull Raster<PixelType> bgImg,
            double x1, double y1, double x2, double y2,
            @NotNull PixelType value) {
        final double rx1 = (x1 + 1) * (bgImg.getWidth() - 1) / 2;
        final double ry1 = (-y1 + 1) * (bgImg.getHeight() - 1) / 2;
        final double rx2 = (x2 + 1) * (bgImg.getWidth() - 1) / 2;
        final double ry2 = (-y2 + 1) * (bgImg.getHeight() - 1) / 2;
        final double k = (ry2 - ry1) / (rx2 - rx1);
        final double q = ry1 - k * rx1;
        final int c1 = (int) rx1;
        final int c2 = (int) rx2;
        return Stream.rangeClosed(c1, c2).foldLeft(bgImg,
            (currentImage, c) ->
                currentImage.withValue(c, (int) (k * c + q), value)
        );
    }
}
