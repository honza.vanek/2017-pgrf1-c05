package rasterops;


import org.jetbrains.annotations.NotNull;
import rasterdata.Raster;

import java.util.Optional;
import java.util.function.Predicate;

public class SeedFill4<T> implements SeedFill<T> {

    @NotNull
    @Override
    public Raster<T> fill(
            @NotNull Raster<T> raster,
            int c, int r, @NotNull T value,
            @NotNull Predicate<T> isInArea) {
        return raster.getValue(c, r).flatMap(
            pixel -> {
                if (isInArea.test(pixel)) {
                    return Optional.of(
                        fill(
                            fill(
                                fill(
                                    fill(
                                        raster.withValue(c, r, value),
                                    c + 1, r, value, isInArea),
                                c, r + 1, value, isInArea),
                            c - 1, r, value, isInArea),
                        c, r - 1, value, isInArea)
                    );
                }
                return Optional.empty();
            }
        ).orElse(raster);
    }
}
