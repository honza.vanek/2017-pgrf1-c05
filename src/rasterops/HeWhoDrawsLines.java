package rasterops;

import org.jetbrains.annotations.NotNull;
import rasterdata.Raster;

public interface HeWhoDrawsLines<PixelType> {
    /**
     * Draws a line described in a [-1;1] square
     * with bottom left image corner in (-1;-1) and top right in (1;1)
     * @param bgImg background image
     * @param x1 x-coordinate of the starting point, in [-1;1]
     * @param y1 y-coordinate of the starting point, in [-1;1]
     * @param x2 x-coordinate of the end point, in [-1;1]
     * @param y2 y-coordinate of the end point, in [-1;1]
     * @param value the value to be set to all line pixels
     * @return new image with the line
     */
   @NotNull Raster<PixelType> draw(
            @NotNull Raster<PixelType> bgImg,
            double x1, double y1, double x2, double y2,
            @NotNull PixelType value);
}
