package rasterops;

import org.jetbrains.annotations.NotNull;
import rasterdata.Raster;

import java.util.function.Predicate;

public interface SeedFill<T> {
    @NotNull Raster<T> fill(
            @NotNull Raster<T> raster,
            int c, int r,
            @NotNull T value,
            @NotNull Predicate<T> isInArea //Function<T, Boolean>
    );
}
