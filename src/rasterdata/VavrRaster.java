package rasterdata;

import io.vavr.collection.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class VavrRaster<PixelType> implements Raster<PixelType> {
    private final @NotNull Vector<PixelType> image;
    private final int width, height;

    public VavrRaster(final int width, final int height,
                      final @NotNull PixelType value) {
        this.width = width;
        this.height = height;
        image = Vector.fill(width * height, () -> value);
    }

    private VavrRaster(final @NotNull Vector<PixelType> image,
                       final int width, final int height) {
        this.image = image;
        this.width = width;
        this.height = height;
    }

    @NotNull
    @Override
    public Optional<PixelType> getValue(int c, int r) {
        if (c >= 0 && r >= 0 && c < getWidth() && r < getHeight())
            return Optional.of(image.get(r * width + c));
        return Optional.empty();
    }

    @NotNull
    @Override
    public Raster<PixelType> withValue(int c, int r, @NotNull PixelType value) {
        if (c >= 0 && r >= 0 && c < getWidth() && r < getHeight())
            return new VavrRaster<>(
                    image.update(r * width + c, value),
                    width, height
            );
        return this;
    }

    @NotNull
    @Override
    public Raster<PixelType> cleared(@NotNull PixelType value) {
        return new VavrRaster<>(width, height, value);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
